import React from 'react';
import Input from './Input';
import Label from './Label';

function FieldSelect({type="", label, value, onChange}) {
  const [
    typeComponent,
    ...typeField
  ] = type.toLocaleLowerCase().split('&');

  switch(typeComponent) {
    case "delete":
      return null;
    case "label":
      return <Label type={typeField.join("&")} label={label} value={value}/>
    case "input":
      return <Input type={typeField.join("&")} label={label} value={value} 
        onChange={onChange} 
      />
    default:
      return <Input type={type} label={label} value={value} 
        onChange={onChange} 
      />
  } 
}

export default FieldSelect;
