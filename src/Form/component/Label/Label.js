import React from 'react';

function Label({label, value}) {
  return (
<div className="form-group row">
    <label htmlFor={label} className="col-sm-3 col-form-label">
      {label}
    </label>
    <div className="col-sm-9">
      <input 
        type="text"
        readOnly
        className="form-control-plaintext"
        id={label}
        defaultValue={value}
      />
    </div>
  </div>
  );
}

export default Label;
