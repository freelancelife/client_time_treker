export default function dateToString(str) {
  return new Date(Number(str)).toLocaleDateString();
}
