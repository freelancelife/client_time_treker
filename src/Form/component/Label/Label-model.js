import React from 'react';
import AuthContext from '../../../AuthContext.js';

class LabelModel extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      row: {},
      error: null
    }
  }

  getModel() {
    const {modelName, id} = this.props;

    this.context.api.view(modelName, id)
    .then(row => this.setState(() => ({row: row[0]})))
    .catch(error => this.setState(() => ({error})));
  }

  componentDidMount() {
    this.getModel();
  }

  render() {
    const {id, label} = this.props;
    const {row} = this.state;

    if (row && row.hasOwnProperty(label||"title")) {
      return row[label||"title"];
    }

    return id;

  }
}

LabelModel.contextType = AuthContext

export default LabelModel;
