import React from 'react';
import Label from './Label.js';
import LabelBoolean from './Label-boolean.js';
import dateToString from './lib/text/dateToString.js';

/*
 *  
 */
function SelectLabel({type="", label, value}) {

  const [inputType] = type.toLocaleLowerCase().split("-");

  switch(inputType) {
    case "date":
      return <Label label={label} value={dateToString(value)}/>
    case "boolean":
      return <LabelBoolean label={label} value={value}/>
    case "model":
    default:
      return <Label label={label} value={value}/>
  }
}

export default SelectLabel;
