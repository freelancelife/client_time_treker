import React from 'react';

function LabelBoolean({label, value=false}) {
 
  return (
    <div className="form-group form-check">
      <input
        type="checkbox"
        className="form-check-input"
        id={label}
        defaultChecked={Boolean(value)}
        readOnly
      />
      <label className="form-check-label" htmlFor={label}>
         {label}
      </label>
    </div>
  );
}

export default LabelBoolean;
