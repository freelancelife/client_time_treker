import React from 'react';

function InputBoolean({label, value=false, onChange}) {

  const handlerChange = ({target}) => {
    onChange(label, Boolean(target.checked));
  }
 
  return (
    <div className="form-group form-check">
      <input
        type="checkbox"
        className="form-check-input"
        id={label}
        checked={value}
        onChange={handlerChange}
      />
      <label className="form-check-label" htmlFor={label}>
        Check {label}
      </label>
    </div>
  );
}

export default InputBoolean;
