import React from 'react';

function InputNumber({label, value, onChange}) {
  const handlerChange = ({target}) => {
    onChange(label, Number(target.value));
  }

  return (
    <div className="form-group">

      <label htmlFor={label}>
        {label} 
      </label>

      <input type="number" 
        className="form-control" 
        id={label} 
        placeholder={`Enter ${label}`}
        value={value}
        onChange={handlerChange}
      />

    </div>
  );
}

export default InputNumber;
