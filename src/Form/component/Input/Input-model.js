import React from 'react';
import AuthContext from '../../../AuthContext';
import IF from '../IF.js';
import { Modal } from '../../../ModalWindow';
import Form from '../../Form'; 
import ModalFormDelete from '../../ModalFormDelete.js';
import ModalFormSelect from '../../ModalFormSelect.js';
import './Input-model.css';

class InputModel extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoadding: false,
      showRows: false,
      showModalAddRow: false,
      showModalEditRow: false,
      showModalDeleteRow: false,
      showModalSelectRow: false,
      deleteRowId: 0,
      desctiption: {keys:[], fields:{}},
      row: {
        title: "Click for select"
      },
      rows: [],
      error: null,
    }

    this.timerId = null;

    this.handlerChange          = this.handlerChange.bind(this);
    this.handleChangeRow        = this.handleChangeRow.bind(this);
    this.handlerSelectRow       = this.handlerSelectRow.bind(this);
    this.handleSelectNull       = this.handleSelectNull.bind(this);
    this.handleAddRow           = this.handleAddRow.bind(this);
    this.handleShowModalAddRow  = this.handleShowModalAddRow.bind(this);
    this.handleShowModalEditRow = this.handleShowModalEditRow.bind(this);
    this.handleShowRowsIsFocus  = this.handleShowRowsIsFocus.bind(this);
    this.handleDeleteRow        = this.handleDeleteRow.bind(this);
    this.handleUpdateRow        = this.handleUpdateRow.bind(this);
    this.findByTitle = this.findByTitle.bind(this);
    this.getRow = this.getRow.bind(this);
  }

  componentDidMount() {
    const api = this.context.api;

    api.desctiption(this.props.modelName)
    .then(body => this.setState(() => ({
      desctiption: body,
      isLoadding: false
    })))
    .then(this.getRow)
    .catch(err => this.setState(() => ({error: err.message})));
  }

  getRow() {
    const { value, modelName } = this.props;

    if (!value) {
      return null;
    }

    return this.context.api.view(modelName, value)
    .then(rows => this.setState(() => ({
      row: (rows.length > 0) ? rows[0] : { title: value }
    })))
    .catch(err => this.setState(() => ({error: err.message})));
  }

  findByTitle(title) {
    const api = this.context.api;
    var where = {};

    if (title) where['title'] = `LIKE-${title}`;

    if (this.props.hasOwnProperty('filter')) {
      where = {...where, ...this.props.filter};
    }

    return api.list(this.props.modelName, where)
    //.then(body => (console.log(body), body))
    .then(body => this.setState(() => ({
      rows: body,
      isLoadding: false
    })))
    .catch(err => this.setState(() => ({error: err.message})));
  }

  handleAddRow() {
    this.setState({isLoadding: true});
    const set = {
      ...this.state.row,
      ...this.props.filter
    }

    return this.context.api.add(this.props.modelName, set)
    .then(() => this.findByTitle(this.state.row.title))
    .then(() => {
      const state = this.state;
      return state.rows.find(row => row.title === state.row.title)
    })
    .then(this.handlerSelectRow)
    .catch(err => this.setState(() => ({error: err.message})));
  }

  handleDeleteRow() {
    return this.context.api
    .del(this.props.modelName, this.state.deleteRowId)
    .then(() => this.setState(() =>({
      showModalDeleteRow: false,
      showModalEditRow: false,
      rows: this.state.rows.filter(row => row.id !== this.state.deleteRowId)
    })))
    .catch(err => this.setState(() => ({error: err.message})));
  }

  handlerChange({target}) {
    const title = target.value;

    // Что бы не отпралять запрос на сервер при вводе одного символа.
    if (this.timerId) {
      clearTimeout(this.timerId);
    }

    this.setState(state => ({row: {title}}));

    const handleTimer = () => {
      this.timerId = null;
      this.findByTitle(title);
    }

    this.timerId = setTimeout(handleTimer, 300);
  }

  handlerShowRows(value) {
    // Сделать запрос данных не при каждом открытии списка rows
    if (value) {
      return this.findByTitle()
      .then(() => this.setState(() => ({
        showRows: true,
      })))
      .catch(err => this.setState(() => ({error: err.message})));
    }

    this.setState(() => ({showRows: value}))
  }

  handleShowRowsIsFocus() {
    return this.findByTitle()
    .then(() => this.setState(() => ({
      showRows: true,
      row: {title: ""}
    })))
    .catch(err => this.setState(() => ({error: err.message})));

  }

  handleShowModalAddRow(value) {
    this.setState(() => ({
      showModalAddRow: value,
      row: {}
    }))
  }

  handleShowModalEditRow(value) {
    if (!this.state.row.hasOwnProperty('id')) {
      value = false; 
    }

    this.setState(() => ({showModalEditRow: value}))
  }

  handleChangeRow(key, value) {
    this.setState(() => ({row: {
      ...this.state.row,
      [key]: value
    }}))
  }

  handlerSelectRow(row) {
    this.setState({
      showRows: false,
      showModalAddRow: false,
      showModalSelectRow: false,
      row
    })

    if(this.props.onChange) {
      this.props.onChange(this.props.label, row.id);
    }
  }

  handleSelectNull() {
    this.setState({
      showRows: false,
      showModalSelectRow: false,
      row: {title: ""}
    })

    if(this.props.onChange) {
      this.props.onChange(this.props.label, undefined);
    }
  }

  handleUpdateRow() {
    return this.context.api
    .update(this.props.modelName, this.state.row, {
      id: this.state.row.id
    })
    .then(() => this.setState(state => ({
      showModalEditRow: false,
      rows: state.rows
        .map(row => (row.id === state.row.id)? state.row: row)
    })))
    .catch(err => this.setState(() => ({error: err.message})));
  }

  render() {
    const { label } = this.props;

    return (
      <div className="form-group mt-2" key={" modal form "+this.props.modelName}>
        <IF status={!!label}>
          <label htmlFor={label}>{label}</label>
        </IF>
        <div className={`input-group ${(this.state.showRows)?"show":""}`}>
          <input type="text"
            id={label}
            className="form-control Filter-input"
            placeholder={`Filter to model list`}
            onChange={this.handlerChange}
            value={this.state.row.title}
            onFocus={this.handleShowRowsIsFocus}
          />

          <div className="input-group-append">
            <button className="btn btn-outline-secondary Filter-btn-show"
              type="button"
              onClick={this.handleShowModalEditRow}
            >
              <i className="bi bi-files"></i>
            </button>
            <button className="btn btn-outline-secondary Filter-btn-show"
              onClick={() => this.handlerShowRows(!this.state.showRows)}
              type="button"
            >
              <i className="bi bi-chevron-down"></i>
            </button>
          </div>

        </div>

        <IF status={this.state.showRows}>
        <div className="card Filter-list">
          <ul className="list-group list-group-flush">
          <IF status={(this.state.rows.length === 0 
            && this.state.row.title !== "")}
          >
            <li className="list-group-item Filter-list-item"
              onClick={this.handleAddRow}
            >
              Create
            </li>
          </IF>
          {this.state.rows.map( row => 
            <li  className="list-group-item Filter-list-item flex justify-content-between"
              key={row.id+row.title}
              onClick={({target}) => {
                if(target.classList.contains('list-group-item')) {
                  return this.handlerSelectRow(row)
                }

                return this.setState(state => ({
                  showModalDeleteRow: true,
                  deleteRowId: row.id
                }))
              }}
            >
              {row.title}
              <i className="bi bi-x"></i>
            </li>
          )}
          </ul>
          <div className="card-footer p-0 flex justify-content-end">
            <div className="btn-group">
              <button className="btn btn-outline-secondary"
                style={{borderRadius: 0}}
                onClick={this.handleSelectNull}
              >
                <i className="bi bi-x"></i>
              </button>
              <button className="btn btn-outline-secondary"
                style={{borderRadius: 0}}
                onClick={() => this.setState(() => ({
                  showModalSelectRow: true 
                }))}
              >
                <i className="bi bi-list-task"></i>
              </button>
              <button className="btn btn-outline-secondary" 
                style={{borderRadius: "0 0 0.25rem 0"}}
                onClick={() => this.handleShowModalAddRow(true)}
              >
                <i className="bi bi-plus"></i>
              </button>
            </div>
          </div>
        </div>
        </IF>
        <IF status={this.state.showModalAddRow}>
          <Modal 
            show={this.state.showModalAddRow} 
            onClick={() => this.handleShowModalAddRow(false)}
          >
            {`Add new ${this.props.modelName}`}
            <Form
              row={this.state.row}
              keys={this.state.desctiption.keys}
              fields={this.state.desctiption.fields}
              onChange={this.handleChangeRow}
            />
            <button className="btn btn-success"
              onClick={this.handleAddRow}
            >Add</button>
          </Modal>
        </IF>
        <IF status={this.state.showModalEditRow}>
          <Modal 
            show={this.state.showModalEditRow} 
            onClick={() => this.handleShowModalEditRow(false)}
          >
            {`Edit ${this.props.modelName}`}
            <Form
              row={this.state.row}
              keys={this.state.desctiption.keys}
              fields={this.state.desctiption.fields}
              onChange={this.handleChangeRow}
            />
            <button className="btn btn-success"
              onClick={this.handleUpdateRow}
            >Update</button>
            <button className="btn btn-danger"
              onClick={() => this.setState(state => ({
                showModalDeleteRow: true,
                deleteRowId: state.row.id
              }))}
            >Delete</button>
          </Modal>
        </IF>
        <IF status={this.state.showModalDeleteRow}>
          <ModalFormDelete
            show={this.state.showModalDeleteRow}
            modelName={this.props.modelName}
            id={this.state.deleteRowId}
            onClose={() => this.setState(() => ({
              showModalDeleteRow: false
            }))}
            onDelete={this.handleDeleteRow}
          />
        </IF>
        <IF status={this.state.showModalSelectRow}>
          <ModalFormSelect
            show={this.state.showModalSelectRow}
            modelName={this.props.modelName}
            rows={this.state.rows}
            onClose={() => this.setState(() => ({
              showModalSelectRow: false
            }))}
            onSelect={this.handlerSelectRow}
          />
        </IF>
      </div>
    );
  }
}

InputModel.contextType = AuthContext;

export default InputModel;
