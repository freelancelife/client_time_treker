import React from 'react';

function Input({label, value, onChange}) {
  const handlerChange = ({target}) => {
    onChange(label, target.value);
  }

  return (
    <div className="form-group">

      <label htmlFor={label}>
        {label} 
      </label>

      <input type="text" 
        className="form-control" 
        id={label} 
        placeholder={`Enter ${label}`}
        value={value}
        onChange={handlerChange}
      />

    </div>
  );
}

export default Input;
