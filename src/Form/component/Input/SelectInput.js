import React from 'react';
import Input from './Input.js';
import InputDate from './InputDate.js';
import InputModel from './Input-model.js'; 
import InputBoolean from './Input-boolean.js';
import InputTextarea from './Input-textarea.js';
import InputNumber from './Input-number.js';

/*
 *  
 */
function SelectInput({type="", label, value, onChange}) {
  const [inputType, modelName] = type.split("&");

  switch(inputType.toLocaleLowerCase()) {
    case "date":
      return <InputDate label={label} value={value} onChange={onChange}/> 
    case "boolean":
      return <InputBoolean label={label} value={value} onChange={onChange}/>
    case "text":
      return <InputTextarea label={label} 
        value={value} onChange={onChange}/>
    case "model":
      return <InputModel label={label} modelName={modelName}
        value={value} onChange={onChange}
      />
    case "number":
      return <InputNumber label={label} value={value} onChange={onChange}/>
    default:
      return <Input label={label} value={value} onChange={onChange}/>
  }
}

export default SelectInput;
