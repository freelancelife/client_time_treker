import React, {useState} from 'react';

function InputDate({label, value, onChange}) {
  const date = new Date(value);
  const [times, setTime] = useState(date.toLocaleTimeString());
  const [dates, setDate] = useState(date
    .toLocaleDateString().split(".").reverse().join("-")
  );

  const handlerChange = ({target}) => {
    if(target.name === "date") {
      setDate(target.value);
    }
    if(target.name === "time") {
      setTime(target.value);
    }

    return onChange(label, new Date(`${dates}T${times}`).getTime())
  }

  return (
    <div className="form-group">
      <label htmlFor={label}>
        date {label} 
      </label>

      <div className="input-group">
        <input
          type="date"
          name={"date"}
          className="form-control"
          id={label}
          placeholder={`Enter ${label}`}
          value={dates}
          onChange={handlerChange}
        />
        <input
          type="time"
          name={"time"}
          className="form-control" 
          id={label} 
          placeholder={`Enter ${label}`}
          value={times}
          onChange={handlerChange}
        />
      </div>

    </div>
  );
}

export default InputDate;
