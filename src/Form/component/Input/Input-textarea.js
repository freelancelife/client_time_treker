import React from 'react';

function InputTextarea({label, value, onChange}) {
  const handlerChange = ({target}) => {
    onChange(label, target.value);
  }

  return (
    <div className="form-group">

      <label htmlFor={label}>{label}</label>

      <textarea 
        className="form-control" 
        id={label}
        rows="3"
        onChange={handlerChange}
        placeholder={`Enter ${label}`}
      >
        {value}
      </textarea>

    </div>
  );
}

export default InputTextarea;
