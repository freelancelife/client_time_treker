import React from 'react';

function Message({ status, text, children }) {
  return (
    <div className={`card-body text-${status} pb-0`}>
      <p className="card-text">{children}</p>
    </div>
  );
}

export default Message;
