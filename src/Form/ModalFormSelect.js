import React from 'react';
import { Modal } from '../ModalWindow';
import List from '../TreeList/List.js';

import AuthContext from '../AuthContext';

class ModalFormSelect extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      rows: [] 
    }
  }
  
  componentDidMount() {
    this.getAll();
  }

  getAll() {
    const api = this.context.api;

    return api.list(this.props.modelName)
    .then(body => this.setState(() => ({
      rows: body,
      isLoadding: false
    })))
    .catch(err => this.setState(() => ({error: err.message})));
  }

  render() {
    const { modelName, show, onClose, onSelect } = this.props;
    const { rows } = this.state;

    return (
      <Modal show={show} onClick={onClose} styleBody={{padding:0}}>
        {`Select ${modelName}`}
        <List modelName={modelName} rows={rows} onDoubleClick={onSelect}/>
      </Modal>
    );
  }
}

ModalFormSelect.contextType = AuthContext;

export default ModalFormSelect;
