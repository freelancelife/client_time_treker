import React from 'react';
import Modal from '../ModalWindow';
import Form from './Form.js';

class FormAdd extends React.Component {
  //constructor(props) {
  //  super(props);
  //}

  render() {
    const { modelName, show, onClose, onChange, onAdd, row={} } = this.props;

    return (
      <Modal 
        show={this.state.showModalAddRow} 
        onClick={() => this.handleShowModalAddRow(false)}
      >
        {`Add new ${this.state.modelName}`}
        <Form
          row={this.state.row}
          keys={this.state.desctiption.keys}
          fields={this.state.desctiption.fields}
          onChange={this.handleChangeRow}
        />
        <button className="btn btn-success"
          onClick={() => this.handleAddRow(this.state.row)}
        >Add</button>
      </Modal>
    );
  }
}

export default FormAdd;
