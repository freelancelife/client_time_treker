import React from 'react';
import { Modal } from '../ModalWindow';

function ModalFormDelete({ modelName, id, show, onClose, onDelete }) {
  return (
    <Modal show={show} onClick={onClose}>
      {`Delete ${modelName} #${id}`}
      <div>
        <p>Are you sure you want to delete these Records?</p>
        <small className="text-danger">This action cannot be undone.</small>
      </div>
      <button className="btn btn-danger"
        onClick={onDelete}
      >Delete</button>
    </Modal>
  );
}

export default ModalFormDelete;
