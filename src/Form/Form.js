import React from 'react';
import SelectField from './component/SelectField.js'; 

function Form({keys=[], fields={}, row={}, onChange}) {

  return (
    <div>
      {
        keys.map(key => 
          <SelectField type={fields[key]}
            key={key + fields[key]}
            label={key}
            value={row[key]}
            onChange={onChange}
          />
        )
      }
    </div>
  );
}

export default Form;
