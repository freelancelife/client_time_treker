import React from 'react';
import Api from './api';

export const initAuth = {
  hostName: 'https://timetraker.herokuapp.com',
  //hostName: 'http://localhost:4005',
  token: null
}

const AuthContext = React.createContext({ api: new Api({
  hostName: initAuth.hostName 
})});


export default AuthContext;
