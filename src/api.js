class Api {
  constructor(props) {
    this.token = props.token || null;
    this.hostName = props.hostName;
  }

  setToken(token) {
    this.token = token;
  }

  fetch(url) {
    return fetch(url)
    .then(response => response.json())
    .then(response => {
      if(response.status !== 'ok') {
        return Promise.reject(response.error);
      }

      return Promise.resolve(response.response);
    });
  }

  fetchPost(url, data) {
    let formBody = `body=${JSON.stringify(data)}`;

    return fetch(url, {
      method: 'POST',
      body: formBody
    })
    .then(response => response.json())
    .then(response => {
      if(response.status !== 'ok') {
        return Promise.reject(response.error);
      }

      return Promise.resolve(response.response);
    });
  }

  register(set) {
    //const data = JSON.stringify({set});
    const data = {set};
    const url = `${this.hostName}?cmd=register&data=${data}`;
    return this.fetchPost(url, data);
  }

  signIn(where) {
    //const data = JSON.stringify({where});
    const data = {where};
    const url = `${this.hostName}?cmd=signin`;
    return this.fetchPost(url, data);
  }

  add(model, set) {
    const data = JSON.stringify({model, set});
    const url = `${this.hostName}?cmd=add&data=${data}&sig=${this.token}`;
    return this.fetch(url);
  }

  update(model, set, where) {
    const data = JSON.stringify({model, set, where});
    const url = `${this.hostName}?cmd=update&data=${data}&sig=${this.token}`;
    return this.fetch(url);
  }

  list(model, where) {
    const data = {model};

    if(!isEmpty(where)) {
      data.where = where;
    }

    const url = `${this.hostName}?cmd=list&data=${JSON.stringify(data)}&sig=${this.token}`;
    return this.fetch(url); 
  }

  view(model, id) {
    const data = JSON.stringify({model, id});
    const url = `${this.hostName}?cmd=view&data=${data}&sig=${this.token}`;
    return this.fetch(url);
  }

  del(model, id) {
    const data = JSON.stringify({model, id});
    const url = `${this.hostName}?cmd=del&data=${data}&sig=${this.token}`;
    return this.fetch(url);
  }

  exportExcel(model, where) {
    const data = JSON.stringify({model, where});
    const url = `${this.hostName}?cmd=excel&data=${data}&sig=${this.token}`;
    console.log('export excel: ', data);
    console.log('url: ', url);
    //return this.fetch(url);
    window.open(url);
    return Promise.resolve(null);
  }

  desctiption(model) {
    const data = JSON.stringify({model});
    const url = `${this.hostName}?cmd=descriptor&data=${data}&sig=${this.token}`;
    return this.fetch(url);
  }
}

function isEmpty(obj) {
    for(var key in obj)
    {
        return false;
    }
    return true;
}

export default Api;

//const api = new Api({
//  cmd:'add',
//  model: 'tasks',
//  hostName: 'http://localhost:4005',
//})
//
//api.fetch({task_client:'motoshar.ru', task_description: 'test'})
//.then(console.lgo)
//.catch(console.log);

