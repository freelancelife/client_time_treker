import React from 'react';
import { FilterDate } from '../calendar';
import '../Page.css';
import AuthContext from '../AuthContext';
import Filter from '../Filter'; 
import Statistics from './Statistics.js';

class PageStatistics extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      tasks: [],
      timers: [],
      prices: {
        task_id: {},
        client_id: {},
        category_id: {}
      },
    }

    this.handleChangeFilter = this.handleChangeFilter.bind(this);
  }

  componentDidMount() {
    this.getTasks()
    .then(() =>this.getPrices())
    .then(() => this.getTimers())
    .catch(console.log);
  }

  handleChangeFilter(key, value) {
    if (this.props.onChangeFilter) {
      this.props.onChangeFilter(key, value);
    }
    
    const filter = {...this.props.filter, [key]:value};
    this.getTasks(filter)
    .then(() => {
      if (this.state.tasks.length === 0) {
        return this.setState(() => ({
          prices: {
            task_id: {},
            client_id: {},
            category_id: {}
          }
        }))
      }

      this.getPrices()
    })
    .then(() => {
      if (this.state.tasks.length === 0) {
        return this.setState(() => ({
          timers: []
        }))
      }

      this.getTimers(filter)
    })
    .catch(console.log);
  }

  getPrices() {
  const filterTask = {
    task_id: this.state.tasks.map(task => task.id)
  };
  const filterClient = {
    client_id: unique(this.state.tasks
      .filter(task => !!task.client_id)
      .map(task => task.client_id))
  };
  const filterCategory = {
    category_id: unique(this.state.tasks
      .filter(task => !!task.category_id)
      .map(task => task.category_id))
  };

    return Promise.all([
      this.context.api.list('price', filterTask)
      .then(res => res.reduce((o, price) =>
        (o[price.task_id] = price, o), {})
      ),
      this.context.api.list('price', filterClient)
      .then(res => res.reduce((o, price) =>
        (o[price.client_id] = price, o), {})
      ),
      this.context.api.list('price', filterCategory)
      .then(res => res.reduce((o, price) =>
        (o[price.category_id] = price, o), {})
      ),
    ])
    .then(response => {
      return this.setState(() => ({
        prices: {
          task_id: response[0],
          client_id: response[1],
          category_id: response[2],
        },
        isLoading: false,
        isError: false
      }));
    })
    .catch(err => 
      this.setState(() => ({isError: true, isLoading: false}))
    )
  }

  getTasks(filter) {
    if (!filter) {
      filter = this.props.filter;
    }

    return this.context.api.list('task', filter)
    .then(response => {
      return this.setState(() => ({
        tasks: response,
        isLoading: false,
        isError: false
      }));
    })
    .catch(err => 
      this.setState(() => ({isError: true, isLoading: false}))
    )
  }

  getTimers(filter) {
    if (!filter) {
      filter = this.props.filter;
    }

    filter = {
      timer_begin_date: filter.date_create,
      task_id: this.state.tasks.map(task => task.id)
    }

    return this.context.api.list('timer', filter)
    .then(response => {
      return this.setState(() => ({
        timers: response,
        isLoading: false
      }));
    })
    .catch(err => 
      this.setState(() => ({isError: true, isLoading: false}))
    )
  }

  render() {
    return (
      <div className="App">
        <div className="Page">
          <div className="Header">
            <nav className="navbar navbar-light bg-light">
              <a className="navbar-brand" href="/">Time Tracker</a>
              <div className="btn-group">
                <button className="btn btn-outline-success"
                  type="button"
                  onClick={() => {
                    this.context.api
                    .exportExcel('task', this.props.filter)
                    .then(console.log)
                    .catch(console.log)
                  }}
                >Export excel</button>
                <button className="btn btn-outline-danger"
                  type="button"
                  onClick={() => this.context.signOff()}
                >Sign out</button>
              </div>
            </nav>
          </div>
          <div className="Nav">
            <FilterDate 
              onChange={date_create => 
                this.handleChangeFilter('date_create', date_create)}
            />
            <Filter
              label={"Filter category"}
              value={this.props.filter.category_id}
              modelName="category"
              onChange={(key, value) =>
                this.handleChangeFilter('category_id', value)}
            />
            <Filter
              label={"Filter client"}
              value={this.props.filter.client_id}
              modelName="client"
              filter={{category_id: this.props.filter.category_id}}
              onChange={(key, value) =>
                this.handleChangeFilter('client_id', value)}
            />
            <Filter
              label={"Set price"}
              modelName="price"
            />
            <button type="button"
              className="btn btn-block btn-outline-primary"
              onClick={() => this.props.setPage('task')}
            >
              list task
            </button>
          </div>
          <div className="Article">
            <Statistics
              filter={this.props.filter}
              timers={this.state.timers}
              tasks={this.state.tasks}
              prices={this.state.prices}
            />
          </div>
        </div>
      </div>
    );
  }
}

function unique(rows) {
  var filter = {};
  return rows.filter(row => {
    if (filter.hasOwnProperty(row)) {
      return false;
    }

    filter[row] = true;
    return true;
  });
}

PageStatistics.contextType = AuthContext;

export default PageStatistics;
