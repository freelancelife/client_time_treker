import React from 'react';
import StatisticsGraph from './StatisticsGraph.js'; 
import AuthContext from '../AuthContext';


class PageStatistics extends React.Component {

  getTimerToTime(timer) {
    const end = (typeof timer.timer_end_date === 'string') ?
      new Date(timer.timer_end_date) : timer.timer_end_date;
    const begin = (typeof timer.timer_begin_date === 'string') ?
      new Date(timer.timer_begin_date) : timer.timer_begin_date;

    return (end - begin)
  }

  Time(time) {
    return { 
      h: Math.trunc(time / 3600000),
      m: Math.trunc((time / 60000) % 60),
      s: Math.trunc((time / 1000) % 60)
    }
  }

  TimeFormatH({h, m}) {
    return (h + (m / 60)).toFixed(2)
  }

  getHoursNumber(time) {
    const {h, m} = this.Time(time);
    return (h + (m / 60))
  }

  getHours(time) {
    return this.TimeFormatH(this.Time(time));
  }

  getData(timers) {
    const workDays = {};

    timers.forEach(timer => {
      const day = new Date(timer.timer_begin_date).getDate();

      if (workDays.hasOwnProperty(day)) {
        workDays[day] += this.getTimerToTime(timer);
      } else {
        workDays[day] = this.getTimerToTime(timer);
      } 
    });

    var data = [];

    const date = this.props.filter.date_create;
    if (date instanceof Array) {
    } else {
      data = this.getDataMonth(date);   
    }

    return data.map(day => {
      if (workDays.hasOwnProperty(day)) {
        return this.createItem(day, Number(this.getHours(workDays[day])));
      }

      return this.createItem(day, 0);
    })
  }

  getDataMonth({from, to}) {
    const date = new Date(from);
    const days = 32 - new Date(date.getFullYear(), date.getMonth(), 32)
    .getDate();

    return new Array(days).fill(0).map( (el,id) => id+1 );
    
  }

  getDataDays(days) {

  }

  createItem(name, time) {
    return {
      name, hours: time, amt: 2400,
    }
  }


  render() {
    const { filter, tasks, timers, prices } = this.props;
    const data    = this.getData(timers);
    const timeAll = timers.reduce((s, timer) => 
      s + this.getTimerToTime(timer), 0); 
    const oTasks = tasks.reduce((o, task) => (o[task.id] = task, o), {});
    const priceAll = timers.reduce((s, timer) => {
      const task = oTasks[timer.task_id];

      if (!task) return s; 

      const getPrice = ({fixed, price}) => {
        if (fixed) {
          return price;
        }
    
        const hours = this.getHoursNumber(this.getTimerToTime(timer));
        return (Number(hours) * Number(price)) + s;
      }

      if (prices.task_id.hasOwnProperty(task.id)) {
        return getPrice(prices.task_id[task.id]);

      } else if (prices.client_id.hasOwnProperty(task.client_id)) {
        return getPrice(prices.client_id[task.client_id]);

      } else if (prices.category_id.hasOwnProperty(task.category_id)) {
        return getPrice(prices.category_id[task.category_id]);
      } 

      return s; 
    }, 0);

    console.log(tasks, timers, prices, timeAll);

    return (
      <div className="card">
        <div className="card-header">
          <Title date={filter.date_create}/>
        </div>
        <div className="card-body">
          <p className="card-text">
            Total tasks: {tasks.length}
          </p>
          <p className="card-text">
            Total time: { this.getHours(timeAll) } h.
          </p>
          <p className="card-text">
            Total money: { priceAll.toFixed(0) } р.
          </p>
        </div>
        <StatisticsGraph data={data}/>
      </div>
    );
  }

}

function Title({date}) {
  var title = 'Statistics';

  if (date instanceof Array) {
    title += `: ${date.map(d => d.split('-').reverse().join('.'))}`;
  } else {
    let from = new Date(date.from).toLocaleDateString();
    let to = new Date(date.to).toLocaleDateString();

    title +=`: from ${from} to ${to}`;
  }

  return title;
}

PageStatistics.contextType = AuthContext;

export default PageStatistics;
