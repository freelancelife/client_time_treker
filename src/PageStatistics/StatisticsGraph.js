import React, { PureComponent } from 'react';
import {
  LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend,
} from 'recharts';

export default class Example extends PureComponent {
  static jsfiddleUrl = 'https://jsfiddle.net/alidingling/xqjtetw0/';
  
  constructor(props) {
    super(props);
    this.state = {
      width: 1000,
    }
  }

  componentDidMount() {
    this.setState(() => ({width: this.container.offsetWidth}));
  }

  render() {
    const { data } = this.props;

    return (
      <div ref={el => (this.container = el)}>
      <LineChart
        width={this.state.width}
        height={300}
        data={data}
        margin={{
          top: 5, right: 30, left: 20, bottom: 5,
        }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="name" />
        <YAxis />
        <Tooltip />
        <Legend />
        <Line type="monotone" dataKey="hours" stroke="#8884d8" activeDot={{ r: 8 }} />
      </LineChart>
      </div>
    );
  }
}
