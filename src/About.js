import React, { useState } from 'react';
import { Modal } from './ModalWindow';
const presentation = "https://docs.google.com/presentation/d/1iYA9qmHtbRuiEecUrXwi8ViMiWkFaKy0dTsK1MsxwQA/edit?usp=sharing"; 
const explanatoryNote = "https://docs.google.com/document/d/1IbuLndxW0JhQYmn_na1NqVQmcmjZERRkyLzbcZFBZQI/edit?usp=sharing";

function About() {
  const [show, handleShow] = useState(false);

  return (
    <div>
      <button className="btn btn-link text-muted" 
        style={{padding: 0}}
        onClick={() => handleShow(true)}
      >
        About
      </button>
      <Modal show={show} onClick={() => handleShow(false)}>
        <h5 className="modal-title">About</h5>
        <div>
          <p>
            The solution was written as part of the final qualification work
          </p>

          <p><a href={explanatoryNote} rel="noopener noreferrer" target="_blank">
            explanatory note for final qualifying work
          </a></p>

          <p><a href={presentation} rel="noopener noreferrer" target="_blank">
            Presentation for final qualification work
          </a></p>

          <p>
            Author: <author>Yolshin Alexey</author>
          </p>
        </div>
      </Modal>
    </div>
  );
}

export default About;
