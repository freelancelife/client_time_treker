import React from 'react';

import './Page.css';

import PageStatistics from './PageStatistics';
import PageTask from './PageTask.js';
const pages = {
  task: PageTask, 
  statistics: PageStatistics
}

function formatTwoNumber(number) {
  return (number > 9) ? number : '0'+number;
}

function formatDate(date) {
  var y = date.getFullYear();
  var m = date.getMonth()+1;
  var d = date.getDate();

  return `${y}-${formatTwoNumber(m)}-${formatTwoNumber(d)}`;
}

class Pages extends React.Component {
  constructor(props) {
    super(props);

    var date = new Date();
    var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
    var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);

    this.state = {
      filter: {
        date_create:{
          from: formatDate(firstDay),
          to: formatDate(lastDay)
        },
      },
      currentPage: 'task'
    }

    this.handleChangeFilter = this.handleChangeFilter.bind(this);
    this.setPage = this.setPage.bind(this);
  }

  componentDidMount() {
  }

  handleChangeFilter(key, value) {
    return this.setState(state => ({
      filter: {
        ...state.filter,
        [key]: value
      }
    }))
  }

  setPage(namePage) {
    return this.setState(() => ({
      currentPage: namePage
    }))
  }

  render() {
    const CurrentPage = pages[this.state.currentPage];
    return <CurrentPage
      {...this.state}
      setPage={this.setPage}
      onChangeFilter={this.handleChangeFilter}
    />
  }
}

//PageTask.contextType = AuthContext;

export default Pages;
