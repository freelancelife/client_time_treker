import React from 'react';

function formatTwoNumber(number) {
  return (number > 9) ? number : '0'+number;
}

function formatDate(date) {
  var y = date.getFullYear();
  var m = date.getMonth()+1;
  var d = date.getDate();

  return `${y}-${formatTwoNumber(m)}-${formatTwoNumber(d)}`;
}

function FormTask({id, task_parent, task_type, task_date, task_client, task_description, onChange}) {
  const date = (task_date) ? new Date(task_date) : new Date();
  return (
    <form key={id}>
      <div className="form-group">
        <label htmlFor="exampleInputTaskParent">Parent</label>
        <input name="formTask-task_parent" type="text" className="form-control" id="exampleInputEmail1" placeholder="Enter parent" value={task_parent} onChange={onChange}/>
      </div>
      <div className="form-group">
        <label htmlFor="exampleInputEmail1">{'Date'}</label>
        <input name="formTask-task_date" type="date" className="form-control" id="exampleInputEmail1" placeholder="Enter date" value={formatDate(date)} onChange={onChange}/>
      </div>
      <div className="form-group">
        <label htmlFor="exampleInputPassword1">Client</label>
        <input name="formTask-task_client" type="text" className="form-control" id="exampleInputPassword1" placeholder="Enter name client" value={task_client} onChange={onChange}/>
      </div>
       <div className="form-group">
          <label htmlFor="exampleFormControlTextarea1">Description</label>
          <textarea name="formTask-task_description" className="form-control" id="exampleFormControlTextarea1" rows="3" onChange={onChange} value={task_description}/>
        </div>
    </form>
  );
}

export default FormTask;
