import React from 'react';
import Tasks from './Tasks';
import {Modal} from '../ModalWindow';
import './Client.Tasks.css';
import FormTask from './FormTask';
import AuthContext from '../AuthContext';
import Form from '../Form';
import IF from '../Form/component/IF.js';

class ClientTasks extends React.Component {
  constructor(props) {
    super(props);

    this.getTasks = this.getTasks.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onChange = this.onChange.bind(this);
    this.onShowTask = this.onShowTask.bind(this);
  }

  onUpdate() {
    return this.context.api
    .update(this.props.modelName, this.props.formTask, {
      id: this.props.formTask.id
    })
    .then(() => this.props.setState(state => ({
      modal: false,
      madalNew: false,
      tasks: state.tasks
        .map(task => (task.id === state.formTask.id)? state.formTask: task)
    })))
    .catch(console.log);
  }

  onDel() {
    return this.context.api
    .del(this.props.modelName, this.props.formTask.id)
    .then(() => this.props.setState(state => ({
      modal: false,
      madalNew: false,
      tasks: state.tasks.filter(task => task.id !== this.props.formTask.id)
    })))
    .catch(console.log);
  }

  onShowTask(target, id) {
    if(target.classList.contains('Timer-btn')) {
      return undefined;
    }

    const task = this.props.tasks
      .find(task => task.id === id);

    this.props.setState(() => ({formTask: task, modal: true, modalNew: false}));
  }

  onChange(key, value) {
    //const keys = target.name.split('-');

    //if(keys.length === 1) {
    //  return this.props.setState(state => ({
    //    [keys[0]]: target.value
    //  }))
    //}

    //if(keys.length === 2) {
    //  return this.props.setState(() => ({
    //    [keys[0]]: {
    //      ...this.props[keys[0]],
    //      [keys[1]]: target.value
    //    }
    //  }));
    //}
    if (this.props.modalNew) {
      return this.props.setState(() => ({
        formTask: {
          ...this.props.formTask,
          [key]: value
        } 
      }))
    }

    return this.props.setState(() => ({
      formTask: {
        ...this.props.formTask,
        [key]: value
      } 
    }))
  }

  getTasks(state) {
    if(!state) {
      state = this.props;
    }

    return this.context.api.list(this.props.modelName, state.filter)
    .then(response => {
      return this.props.setState(() => ({
        ...state,
        tasks: response,
        loading: false,
        error: false
      }));
    })
    .catch(err => 
      this.props.setState(() => ({...state, error: true, loading: false}))
    )
  }

  onSubmit(set) {
    for(var key in set) {
      if(set[key] === '') {
        return null;
      }
    }
    this.context.api.add(this.props.modalNew, set)
    .then(() => this.getTasks({...this.props, modal: false}))
    .catch(console.log);
  }

  componentDidMount() {
    this.getTasks();
  }

  render() {
    console.log(this.props);
    return (
      <div>
        <Tasks
          tasks={this.props.tasks}
          error={this.props.isError}
          loading={this.props.isLoading}
          onShowTask={this.onShowTask}
        />
        <IF status={this.props.modalNew && this.props.modal}>
          <Modal show={this.props.modal} onClick={this.props.onCloseModal}>
            <h5 key="title" className="modal-title">New task</h5>
            <Form
              row={this.props.formTask}
              keys={this.props.keys}
              fields={this.props.fields}
              onChange={this.onChange}
            />
            <button key="add" 
              onClick={() => this.onSubmit(this.props.formTask)}
              className="btn btn-success"
            >Add</button>
          </Modal>
        </IF>
        <IF status={!this.props.modalNew && this.props.modal}>
          <Modal show={this.props.modal} onClick={this.props.onCloseModal}>
            <h5 key="title" className="modal-title">Show task</h5>
            <Form
              row={this.props.formTask}
              keys={this.props.keys}
              fields={this.props.fields}
              onChange={this.onChange}
            />
            <button key="delete" 
              onClick={() => this.onDel()} 
              className="btn btn-danger"
            >Delete</button>,
            <button key="update" 
              onClick={() => this.onUpdate()} 
              className="btn btn-success"
            >Update</button>
          </Modal>
        </IF>
      </div>
    );
  }
}

ClientTasks.contextType = AuthContext;

export default ClientTasks;
