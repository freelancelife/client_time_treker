import React from 'react';
import AuthContext from '../AuthContext';

class GroupTask extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      groups: []
    }
    
    this.onChange = this.onChange.bind(this);
  }

  componentDidMount() {
    this.context.api.list('tasks', {task_parent: null})
    .then(groups => this.setState(state => ({
      groups 
    })))
  }

  onChange({target}) {
    let value = (target.value === 'null') ? null : target.value;
    this.props.onChange(value);
  }

  render() {
    console.log('GroupTask: ', this.state.groups);
    return (
      <select className="form-control"
        name='filter-task_parent'
        onChange={this.onChange}
        value={String(this.props.value)}
      >
        <option value="null">Группы</option>
    {this.state.groups.map(group =>
      <option key={group.task_client} value={group.task_client}>{group.task_client}</option>)}
      </select>
    );
  }
}

GroupTask.contextType = AuthContext;

export default GroupTask;
