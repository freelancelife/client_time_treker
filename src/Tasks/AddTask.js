import React from 'react';
import './AddTask.css';
import {Client} from '../Client';
import FormTask from './FormTask';

class AddTask extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      task_date: new Date().toISOString().split('T')[0],
      task_client: '',
      task_description: ''
    }

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChange({target}) {
    this.setState(() => ({[target.name]: target.value}));
  }

  onSubmit() {
    this.props.onSubmit(this.state);
    this.setState(() => ({
      task_date: new Date().toISOString().split('T')[0],
      task_client: '',
      task_description: ''
    }));
  }

  render() {
    return [
      <h5 className="modal-title">New task</h5>,
      <FormTask/>,
      <button className="btn btn-success">Add</button>
    ];
  }
}

export default AddTask;
