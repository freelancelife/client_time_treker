import React from 'react';
import './Tasks.css';
import Task from './Task';

function Tasks({tasks, error, loading, onShowTask}) {
  var tasksEl = [];
  while (true) {
    if(error) {
      tasksEl.push(<div key='TasksError' className="TasksError">Error loading tasks</div>); 
      break;
    }
  
    if(loading) {
      tasksEl.push(<div key='TakssLoading' className="TakssLoading">... Loading tasks</div>); 
      break;
    }

    if(tasks && tasks.length > 0) {
      tasksEl = tasks.map(task => <Task onClick={({target}) => onShowTask(target, task.id)}  key={task.id} {...task}/>);
    } else {
      tasksEl.push(<div key='NoTasks' className="NoTasks">No tasks</div>); 
    }
    break;
  }

  return (
    <ul className="list-gruop">
      <li className="list-group-item" style={{padding: '0px'}}>
        <div className="Task">
          <div className="Task-date Task-heade-item-first" >{'Date'}</div>
          <div className="Task-client" >Client</div>
          <div className="Task-description" >Description</div>
          <div className="Task-timer Task-heade-timer" >Timer</div>
        </div>
      </li>
        {tasksEl}
    </ul>
  );
}

export default Tasks;
