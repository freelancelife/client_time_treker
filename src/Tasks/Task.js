import React from 'react';
import './Task.css';
import {Timer} from '../Timer';
import LabelModel from '../Form/component/Label/Label-model.js';

function Task({id, date_create, client_id, title, onClick}) {
  return (
    <li onDoubleClick={onClick}  className="list-group-item list-group-item-action" style={{padding: '0px', cursor: "pointer"}}>
      <div key={id} className="Task">
        <div className="Task-date" >{new Date(date_create).toLocaleDateString()}</div>
        <div className="Task-client" >
          <LabelModel modelName="client" id={client_id}/>
        </div>
        <div className="Task-description" >{title}</div>
        <div className="Task-timer" >
          <Timer task_id={id}/>
        </div>
      </div>
    </li>
  );
}

export default Task;
