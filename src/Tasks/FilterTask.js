import React from 'react';
import './FilterTask.css';

class FilterTask extends React.Component {
  render() {
    return (
      <div className="input-group mb-3">
        <div className="input-group-prepend">
          <span className="input-group-text" id="basic-addon1">Filter</span>
        </div>
        <input type="date" name="filter-task_date-from"
          className="form-control"
          onChange={this.props.onChange}
          value={this.props.from}/>
        <input type="date" name="filter-task_date-to"
          className="form-control"
          onChange={this.props.onChange}
          value={this.props.to}/>
        {this.props.children}
      </div>
    );
  }
}

export default FilterTask;
