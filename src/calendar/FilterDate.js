import React from 'react';
import Month from './Month';
import { formatDate } from './core';

class FilterDate extends React.Component {
  constructor(props) {
    super(props);
    
    this.state = {
      currentDate: new Date(),
      select: [],
    }

    this.onChangeFilter = this.onChangeFilter.bind(this);
  }

  getFirstAndLastMonth(date) {
    return { 
      from: new Date(date.getFullYear(), date.getMonth(), 1),
      to: new Date(date.getFullYear(), date.getMonth() + 1, 0),
    }

  }

  onChangeFilter(value) {
      var state = this.state;
      var newState = {};
      var date = state.currentDate;

      switch(value) {
        case 'month':
          newState = {
            currentDate: new Date(),
            select: []
          }
          break;
        case 'next':
          newState = {
            ...state,
            currentDate: new Date(date.getFullYear(), date.getMonth() + 1)
          }
          break;
        case 'previous':
          newState = {
            ...state,
            currentDate: new Date(date.getFullYear(), date.getMonth() - 1)
          }
          break;
        default: {
          if(!isNaN(Number(value))) {
            let year = date.getFullYear();
            let month = date.getMonth();
            let day = Number(value);
            let d = formatDate(new Date(year, month, day))
            //let d = `${year}-${month}-${day}`
            //new Date(date.getFullYear(), date.getMonth(), Number(value))

            newState = {
              select: (state.select.includes(d)) ? state.select.filter(el => el !== d) : [...state.select, d]
              //select: [...state.select, Number(value)]
            }
          } else {
            newState = {};
          }
        }
      }

      if(this.props.onChange) {
        if(newState.select.length === 0) {
          let date = (newState.currentDate) ? newState.currentDate: state.currentDate;
          this.props.onChange(this.getFirstAndLastMonth(date));
        } else {
          this.props.onChange(newState.select);
        }
      }

      this.setState(state => newState);
  }

  render() {
    return (
      <Month 
        onChange={this.onChangeFilter}
        date={this.state.currentDate}
        select={this.state.select}
      />
    );
  }
}

export default FilterDate;
