import React from 'react';
import './Month.css';
import {getMonth, formatDate} from './core';

function Days({className, days, selectDay, date, currentMonth, currentDay, onClick}) {
  return days.map((el, id) => { 
    if(currentMonth && currentDay === el) {
      return <div className={className} key={id}>
        <div className="curretnDay" onClick={() => onClick(el)}>{el}</div>
      </div>
    }

    if(selectDay && date) {
      let d = formatDate(new Date(date.getFullYear(), date.getMonth(), el))
      if(selectDay.includes(d)) { 
        return <div className={className} key={id}>
          <div className="selectDay" onClick={() => onClick(el)}>{el}</div>
        </div>
      }
    } 

    return <div className={className} key={id} onClick={() => onClick(el)}>{el}</div>
    }
  );
}

function Month({date, select, onChange}) {
  date = new Date(date.getFullYear(), date.getMonth());
  let calendar = getMonth(date);

  return(
    <div>
      <div className="Month-title">
      <div onClick={() => onChange('month')}>{date.toLocaleString('ru', {year:'numeric', month: 'long'})}</div>
      <div>
        <button onClick={() => onChange('previous')} className="btn">{'<'}</button>
        <button onClick={() => onChange('next')} className="btn">{'>'}</button>
      </div>
      </div>
      <div className="gridMont">
        <Days onClick={()=>{}} className="otherTextStyle" days={calendar[0]}/> 
        <Days onClick={()=>{}} className="otherTextStyle" days={calendar[1]}/> 
        <Days onClick={onChange} days={calendar[2]} 
          currentMonth={calendar[4]} 
          currentDay={calendar[5]}
          selectDay={select}
          date={date}
        /> 
        <Days onClick={()=>{}} className="otherTextStyle" days={calendar[3]}/> 
      </div>
    </div>
  )
}


export default Month;

