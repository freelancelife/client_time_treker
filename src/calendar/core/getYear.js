const daysInMonth = require('./daysInMonth');

//const currentDate = new Date();

function getMonth(date) {
  let currentDate = new Date();
  let currentMonth = currentDate.getMonth() === date.getMonth();
  let currentDay = (currentMonth) ? new Date().getDate() : 0; 

  return {
    date,
    daysInMonth: daysInMonth(date),
    currentMonth,
    currentDay
  }
}

function getYear(date) {
   
  return new Array(12)
    .fill()
    .map((el, id) => getMonth(new Date(date.getFullYear(), id, 1)));
}

console.time('getYear');
console.log(getYear(new Date()));
console.timeEnd('getYear');

module.exports = getYear;
