module.exports = {
  getMonth: require('./getMonth'),
  formatDate
}

function formatTwoNumber(number) {
  return (number > 9) ? number : '0'+number;
}

function formatDate(date) {
  var y = date.getFullYear();
  var m = date.getMonth()+1;
  var d = date.getDate();

  return `${y}-${formatTwoNumber(m)}-${formatTwoNumber(d)}`;
}
