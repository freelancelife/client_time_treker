
function daysInMonth(date) {
  return 32 - new Date(date.getFullYear(), date.getMonth(), 32)
    .getDate();
};

module.exports = daysInMonth;
