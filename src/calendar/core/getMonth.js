const daysInMonth = require('./daysInMonth');
const title = ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'];

/**
 * return @ array 49 el
 * */
function get(date) {
  const dayWeek = date.getDay();
  const namberDaysCurrentMonth = daysInMonth(date);
  const namberDaysLastMonth = daysInMonth(new Date(date.getFullYear(), date.getMonth() - 1)); 
  const daysNext = 42 - (dayWeek + namberDaysCurrentMonth);

  let currentDate = new Date();
  let currentMonth = currentDate.getMonth() === date.getMonth();
  let currentDay = (currentMonth) ? new Date().getDate() : 0; 

  return [
    title,

    new Array(dayWeek)
    .fill()
    .map((el,id) => namberDaysLastMonth - id)
    .reverse(),

    new Array(namberDaysCurrentMonth)
    .fill()
    .map((el, id) => id + 1),

    new Array(daysNext)
    .fill()
    .map((el,id) => 1 + id),

    currentMonth,
    currentDay
  ] 
}

module.exports = get;
