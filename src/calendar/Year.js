import React from 'react';
import './Year.css';
import Month from './Month';

function Year({date}) {
  return(
    <div className="gridYear">
      <Month date={new Date(date.getFullYear(), 0)}/>
      <Month date={new Date(date.getFullYear(), 1)}/>
      <Month date={new Date(date.getFullYear(), 2)}/>
      <Month date={new Date(date.getFullYear(), 3)}/>
      <Month date={new Date(date.getFullYear(), 4)}/>
      <Month date={new Date(date.getFullYear(), 5)}/>
      <Month date={new Date(date.getFullYear(), 6)}/>
      <Month date={new Date(date.getFullYear(), 7)}/>
      <Month date={new Date(date.getFullYear(), 8)}/>
      <Month date={new Date(date.getFullYear(), 9)}/>
      <Month date={new Date(date.getFullYear(), 10)}/>
      <Month date={new Date(date.getFullYear(), 11)}/>
    </div>
  )
}


export default Year;

