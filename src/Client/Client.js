import React from 'react';

class Client extends React.Component {
  render() {
    return (
      <div className="form-group">
        <label htmlFor="exampleInputTaskParent">Parent</label>
        <input name="formTask-task_parent" type="text" className="form-control" id="exampleInputEmail1" placeholder="Enter parent"  list="browsers" name={this.props.name} value={this.props.value} onChange={this.props.onChange}/>
        <datalist id="browsers">
          <option value="All"/>
          <option value="motoshar.ru"/>
          <option value="1C"/>
        </datalist>
      </div>
    );
  }
}

export default Client;
