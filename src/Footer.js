import React from 'react';
import './Footer.css';
import About from './About.js';
const hrefUserGuides = "https://docs.google.com/document/d/1HBF-IrXkgEDs6zOd-cR_U8S_ZAORNFY3ecJrnv0S0zk/edit?usp=sharing"; 

function Footer() {
  return (
    <footer className="footer mt-auto py-3">
      <div className="container d-flex justify-content-between">
        <a href={hrefUserGuides} className="text-muted" rel="noopener noreferrer" target="_blank">
          User guides
        </a>
        <About/>
      </div>
    </footer>
  );
}

export default Footer;
