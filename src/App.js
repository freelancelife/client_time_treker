import React from 'react';
import Auth from './Auth';
import './icons.css';
import Pages from './Pages.js';
import Footer from './Footer.js';

class App extends React.Component {
  render() {
    return [
        <main className="flex-shrink-0">
          <Auth>
            <Pages/>
          </Auth>
        </main>,
        <Footer/>
    ];
  }

}

export default App;
