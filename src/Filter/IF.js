function IF({status, children}) {
  if(status) {
    return children;
  } else {
    return null;
  }
}

export default IF;
