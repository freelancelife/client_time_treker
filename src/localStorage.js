export function setItem(key, value) {
  if(!window.localStorage) {
    return null;
  }

  localStorage.setItem(key, value);
}

export function getItem(key) {
  if (!window.localStorage
  && !window.localStorage.length) {
    return null;
  }

  return localStorage.getItem(key);
}

export function removeItem(key) {
  if (!window.localStorage) {
    return false;
  }

  return localStorage.removeItem(key);
} 
