import React, {useState, useEffect} from 'react';

import List from './List.js';

function TreeList({model, rows, onShow,
  isDidInvalidate, isInitialized, isLoading, isUseFilter, filter,
  getAll, getDescription, getFilterRows})
{
  useEffect(() => {
    if (!isLoading && isDidInvalidate && !isInitialized) {
      return getDescription();
    }
    if (!isLoading && isDidInvalidate && isUseFilter) {
      return getFilterRows(filter); 
    }
    if (!isLoading && isDidInvalidate && isInitialized) {
      return getAll();
    }
  })

  if(isLoading) {
    return <div>Loading ...</div>
  }

  return (
    <div className="card">
      <div className="card-header flex justify-content-between">
        {model} list
        <button className="btn btn-success"
          onClick={() => onShow(model)}
        >
          Add
        </button>
      </div>
      <List modelName={model} rows={rows}/>
    </div>
  );
}

export default TreeList;
