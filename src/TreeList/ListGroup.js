import React, {useState} from 'react';
import Actions from './component/Actions.js';
import List from './List';
import IF from '../Form/component/IF.js';

function ListGroup({rows=[], row={}, modelName}) {
  const [show, setShow] = useState(false);
  return [
    <li key={row.id} className="list-group-item flex justify-content-between ">
      <i className={`bi bi-btn ${(show)?"bi-chevron-down":"bi-chevron-right"}`} 
        onClick={() => setShow(!show)}>
      </i>
      {row.title}
      <Actions modelName={modelName} id={row.id} row={row} />
    </li>,
    <IF key={row.id+"list"} status={show}>
      <List  rows={rows} parent={row.id} modelName={modelName}/>
    </IF>
  ];
}

export default ListGroup;
