import { connect } from 'react-redux';
import { 
  modalShow, 
  modalFormShow, 
  modalList 
} from '../../Modal/actions.js';
import Actions from './Actions.js';

const mapStateToProps = (state, props) => {
  return {
  }
}

const mapDispatchToProps = (dispatch, props) => {
  const {modelName, id, row} = props;

  return {
    onShow: () => {
      dispatch(modalFormShow(modalList[0], modelName, id))
    },
    onEdit: () => {
      dispatch(modalFormShow(modalList[1], modelName, id, row))
    },
    onDelete: () => {
      dispatch(modalShow(modalList[2], modelName, id))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Actions);
