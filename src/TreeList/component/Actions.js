import React, {useState} from 'react';
import IF from '../../Form/component/IF.js';

function Actions({onShow, onEdit, onDelete}) {
  const [show, setShow] = useState(false);

  return (
    <div className="bi-container">
      <i className={`bi bi-btn ${(show) ?
        "bi-chevron-right":"bi-chevron-left"}`} 
        onClick={() => setShow(!show)}
      ></i>
      <IF status={show}>
        <i className="bi bi-btn bi-pencil"
          onClick={onEdit}
        ></i>
        <i className="bi bi-btn bi-plus"
          onClick={onShow}
        ></i>
        <i className="bi bi-btn bi-trash"
          onClick={onDelete}
        ></i>
      </IF>
    </div>
  );
}

export default Actions;
