import React from 'react';
import ListGroup from './ListGroup.js';
import Actions from './component/Actions.js';

function List({rows=[], parent=0, modelName, onDoubleClick}) {
  /*
   * Отображение списка
   * row {id, title}
   */
  if (rows.length !== 0 && (!rows[0].hasOwnProperty('parent') || !rows[0].hasOwnProperty('isChildren')))
  {
    return (
      <ul className="list-group list-group-flush">
      {rows.map(row => 
        <li key={row.id.toString()}
          className="list-group-item flex justify-content-between"
          onDoubleClick={() => onDoubleClick(row)}
        >
          {row.title}
          <Actions modelName={modelName} id={row.id} row={row} />
        </li>
      )}
      </ul>
    );
  }
  /*
   * Отображение древовидного списка
   * row {id, title, parent, isChildren}
   * parent = id родителя
   * idChildren = boolean если у елемента вложенные элементы
   */
  return (
    <ul className="list-group list-group-flush">
      {
        rows.filter(row => row.parent === parent)
        .map(row => (row.isChildren) ?
          <ListGroup key={row.id+"listGroup"}
            rows={rows}
            row={row}
            modelName={modelName}
            onDoubleClick={onDoubleClick}
          /> : 
          <li key={row.id.toString()} 
            className="list-group-item flex justify-content-between"
            onDoubleClick={() => onDoubleClick(row)}
          >
            {row.title}
            <Actions modelName={modelName} id={row.id} row={row} />
          </li>
        )
      }
    </ul>
  );
}

export default List;
