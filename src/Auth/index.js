import React from 'react';
import SignIn from './SignIn';
import Register from './Register';
import AuthContext from '../AuthContext';
import Api from '../api';
import {initAuth} from '../AuthContext'
import {getItem, setItem, removeItem} from '../localStorage';

class Auth extends React.Component {
  constructor(props) {
    super(props);

    let token = null;
    let remember = (getItem('remember') === "true") ? true : false;
    
    if(!remember) {
      removeItem('token');
    } else {
      token = getItem('token');
    }

    this.state = {
      isLoadding: false,
      error: null, 
      user_name: '',
      password: '',
      remember: remember,
      status: 'signin',
      api: new Api({
        hostName: initAuth.hostName,
        token: token || initAuth.token
      }),
      authorized: (remember && token) ? true : false,
    }

    this.register = this.register.bind(this);
    this.signIn = this.signIn.bind(this);
    this.signOff = this.signOff.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  signOff() {
    removeItem('token');
    removeItem('remember');

    this.setState(() => ({
      authorized: false
    }))
  }

  onChange({target}) {
    this.setState(() => {
      if(target.hasOwnProperty('checked')) {
        return {
          [target.name] : target.checked
        }
      }
      return {
        [target.name] : target.value
      }
    })
  }

  signIn() {
    this.setState({
      isLoadding: true,
      error: null
    });

    return this.state.api.signIn({
      user_name: this.state.user_name,
      password: this.state.password
    })
    .then(({token}) => {
      setItem('token', token);
      setItem('remember', this.state.remember);

      this.setState(() => ({
        api: new Api({
          hostName: initAuth.hostName,
          token
        }),
        authorized: (token) ? true : false,
        isLoadding: false,
        error: null
      }))
    })
    .catch(err => this.setState(() => ({
      isLoadding: false,
      error: 'Login failed' 
    })));
  }

  register() {
    return this.state.api.register({
      user_name: this.state.user_name,
      password: this.state.password,
      email: this.state.email,
      type: 'user'
    })
    .then(({token}) => this.signIn(token))
    .catch(console.log);
  }

  render() {
    if(this.state.authorized) {
      return (
        <AuthContext.Provider value={{
          api: this.state.api,
          signOff: this.signOff
        }}>
          {this.props.children};
        </AuthContext.Provider>
      )
    }

    if(this.state.status === 'signin') {
      return <SignIn {...this.state}
        register={() => this.setState(() => ({status: 'register'}))}
        signin={this.signIn}
        onChange={this.onChange} />
    }

    if(this.state.status === 'register') {
      return <Register {...this.state}
        signin={() => this.setState(() => ({status: 'signin'}))}
        register={this.register}
        onChange={this.onChange} />
    }

  }

}

Auth.contextType = AuthContext;

export default Auth;
