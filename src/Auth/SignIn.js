import React from 'react';
import './SignIn.css';

function SignIn({signin, register, onChange, user_name, password, remember,
  isLoadding, error
}) 
{
  return (
    <div className="form-signin"
      onKeyPress={event => (event.key === 'Enter') ? signin() : null}
    >
      <div className="text-center mb-4">
        <h1 className="h3 mb-3 font-weight-normal">Time Tracker</h1>
      </div>

      <div className="form-group">
        <label htmlFor="InputUsername">Username</label>
        <input type="text"
          className="form-control"
          id="InputUsername"
          aria-describedby="usernameHelp"
          placeholder="Username"
          name="user_name"
          onChange={onChange}
          value={user_name}
        />
      </div>
      <div className="form-group">
        <label htmlFor="exampleInputPassword1">Password</label>
        <input type="password"
          className="form-control"
          id="exampleInputPassword1"
          placeholder="Password"
          name="password"
          onChange={onChange}
          value={password}
        />
      </div>
      <div className="form-group form-check">
        <input type="checkbox"
          className="form-check-input"
          id="exampleCheck1"
          name="remember"
          onChange={onChange}
          checked={remember}
        />
        <label className="form-check-label" htmlFor="exampleCheck1">Remember me</label>
      </div>
      <div className="btn-group btn-group-lg btn-group-form" role="group" aria-label="Basic example">
        <button className="btn btn-outline-primary"
          type="submit"
          onClick={register}
        >Register</button>
        <button className="btn btn-primary"
          type="submit"
          onClick={signin}
        >Sign in</button>
      </div>

      {(isLoadding) ?
        <div className="alert alert-success mt-3 text-center" role="alert">
          Loadding ...
        </div> : null
      }

      {(error) ?
        <div className="alert alert-danger mt-3 text-center" role="alert">
          {error}
        </div> : null
      }

      <p className="mt-5 mb-3 text-muted text-center">
        &copy; 2019 - {new Date().getFullYear()}
      </p>
    </div>
  );
}

export default SignIn;
