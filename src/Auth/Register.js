import React, { useState } from 'react';
import './SignIn.css';

function Register({signin, register, onChange, email, user_name, password, remember}) {
  const [isMyPersonalData, setMyPersonalData] = useState(false);
  const handleMyPersonalData = ({target}) => {
    setMyPersonalData(target.checked);
  } 

  return (
    <div className="form-signin"
      onKeyPress={event => (event.key === 'Enter') ? register() : null}
    >
      <div className="text-center mb-4">
        <h1 className="h3 mb-3 font-weight-normal">Time Tracker</h1>
      </div>

      <div className="form-group">
        <label htmlFor="InputEmail">Email</label>
        <input type="email"
          className="form-control"
          id="InputEmail"
          aria-describedby="emailHelp"
          placeholder="Email"
          name="email"
          onChange={onChange}
          value={email}
        />
      </div>

      <div className="form-group">
        <label htmlFor="InputUsername">Username</label>
        <input type="text"
          className="form-control"
          id="InputUsername"
          aria-describedby="usernameHelp"
          placeholder="Username"
          name="user_name"
          onChange={onChange}
          value={user_name}
        />
      </div>
      <div className="form-group">
        <label htmlFor="exampleInputPassword1">Password</label>
        <input type="password"
          className="form-control"
          id="exampleInputPassword1"
          placeholder="Password"
          name="password"
          onChange={onChange}
          value={password}
        />
      </div>
      <div className="form-group form-check">
        <input type="checkbox"
          className="form-check-input"
          id="exampleCheck1"
          name="remember"
          onChange={onChange}
          checked={remember}
        />
        <label className="form-check-label" htmlFor="exampleCheck1">Remember me</label>
      </div>
      <div className="form-group form-check">
        <input type="checkbox"
          className="form-check-input"
          id="isMyPersonalData"
          name="isMyPersonalData"
          onChange={handleMyPersonalData}
          checked={isMyPersonalData}
        />
        <label className="form-check-label" htmlFor="isMyPersonalData">
          I agree to the processing of my personal data
        </label>
      </div>
      <div className="btn-group btn-group-lg btn-group-form" role="group" aria-label="Basic example">
        <button className="btn btn-primary"
          type="submit"
          onClick={register}
          onKeyDown={event => (event.key === 'Enter') ? register() : null}
          disabled={!isMyPersonalData}
        >Register</button>
        <button className="btn btn-outline-primary"
          type="submit"
          onClick={signin}
        >Sign in</button>
      </div>
      <p className="mt-5 mb-3 text-muted text-center">
        &copy; 2019 - {new Date().getFullYear()}
      </p>
    </div>
  );
}

export default Register;
