import React from 'react';
import './ModalWindow.css';

class Modal extends React.Component {
  constructor(props) {
    super(props);

    this.onClose = this.onClose.bind(this);
  } 
  onClose({target}) {
    if(target.classList.contains('modal')) {
      return this.props.onClick();
    }
  }
  render() {
      if(this.props.show) {
        const { styleBody={} } = this.props;
        const [title, body, ...buttons] = this.props.children;

        return [
          <div className="modal" style={{display: 'block'}} role="dialog" onClick={this.onClose}>
            <div className="modal-dialog modal-dialog-scrollable" role="document">
              <div className="modal-content">
                <div className="modal-header">
                  {title}
                  <button onClick={this.props.onClick} type="button" className="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div className="modal-body" style={styleBody}>
                  {body}
                </div>
                <div className="modal-footer">
                  <button key="close" type="button" className="btn btn-secondary" data-dismiss="modal" onClick={this.props.onClick}>Close</button>
                  {buttons}
                </div>
              </div>
            </div>
          </div>,
          <div className="modal-backdrop" >
          </div>
        ];
      } else {
        return null;
      }
  }
}

export default Modal;
