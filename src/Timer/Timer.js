import React from 'react';
import './Timer.css';
import AuthContext from '../AuthContext';

class Timer extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      run: false,
      timer_begin_date: null,
      timer_end_date: null, 
      time: 0,
      timerId: null,
      extraTime: 0,
      timers: [],
      modelName: 'timer',
      defoultTitle: ''
    }

    this.timerId = null;

    this.onClick = this.onClick.bind(this);
  }

  componentDidMount() {
    const { modelName } = this.state;

    this.context.api.list(modelName, {task_id: this.props.task_id})
    .then(response => this.setState(() => ({timers: response, extraTime: this.getExtraTime(response)})))
    .catch(console.log);
  }

  componentWillUnmount() {
    clearInterval(this.timerId);
    document.title = `Time Tracker`;
    if (this.state.run && this.state.time >= 60000) {
      this.context.api.add(this.state.modelName, {
        task_id: this.props.task_id,
        timer_begin_date: this.state.timer_begin_date,
        timer_end_date: new Date() 
      });
    }
  }

  onClick() {
    this.setState(state => {
      const newState = {};

      if(state.run) {
        clearInterval(this.timerId);
        newState.run = false; 
        newState.timer_end_date = new Date();

        document.title = `Time Tracker`;

        if(state.time >= 60000) {
          newState.timers = [
            {timer_begin_date: state.timer_begin_date, timer_end_date: newState.timer_end_date},
            ...state.timers
          ];
          newState.extraTime = this.getExtraTime(newState.timers);
          newState.time = 0;

          //console.log('Timer onClick: ',newState.timer_end_date);
          this.context.api.add(this.state.modelName, {
            task_id: this.props.task_id,
            timer_begin_date: state.timer_begin_date,
            timer_end_date: newState.timer_end_date
          })
        }

        return newState;
      }

      this.timerId = setInterval(() => {
        const time = this.getTime();
        this.setState(() => ({time}));
        document.title = `Time Tracker ${this.TimeFormat(this.time(time))}`;
      }, 200);

      return {run: true, timer_begin_date: new Date(), timer_end_date: null}
    })
  }

  getExtraTime(timers) {
    return timers.reduce((sum, timer) => {
      const end = (typeof timer.timer_end_date === 'string') ?
        new Date(timer.timer_end_date) : timer.timer_end_date;
      const begin = (typeof timer.timer_begin_date === 'string') ?
        new Date(timer.timer_begin_date) : timer.timer_begin_date;

      return sum + (end - begin)
    }, 0);
  }

  getTime() {
    if(this.state.timer_begin_date === null) {
      return 0;
    }

    if(this.state.timer_end_date === null) {
      return (new Date() - this.state.timer_begin_date);
    }

    return (this.state.timer_end_date - this.state.timer_begin_date);
  }

  time(time) {
    return { 
      h: Math.trunc((time / 3600000) % 60),
      m: Math.trunc((time / 60000) % 60),
      s: Math.trunc((time / 1000) % 60)
    }
  }

  TimeFormat({h,m,s}) {

    h = h > 9 ? h : '0'+h;
    m = m > 9 ? m : '0'+m;
    s = s > 9 ? s : '0'+s;

    return `${h}:${m}:${s}`;
  }

  TimeFormatH({h, m}) {
    return (h + (m / 60)).toFixed(2)
  }

  render() {
    var time = this.time(this.state.time + this.state.extraTime);
    var className = (this.state.run) ? 'Timer-btn Timer-btn-danger': 'Timer-btn Timer-btn-success';
    return (
      <div className="Timer">
        <button className={className} onClick={this.onClick}>
          {this.TimeFormat(time)} - {this.TimeFormatH(time)}
        </button>
      </div>
    );
  }

}

Timer.contextType = AuthContext

export default Timer;
