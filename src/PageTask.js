import React from 'react';
import { Tasks } from './Tasks';
import { FilterDate } from './calendar';
import './Page.css';
import AuthContext from './AuthContext';
import Filter from './Filter'; 
import Form from './Form';
import IF from './Form/component/IF.js';
import {Modal} from './ModalWindow';
import ModalFormDelete from './Form/ModalFormDelete.js';

class PageTask extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoadding: false,
      isError: false,
      clients: [],
      tasks: [],
      timers: [],
      modal: false,
      modalNew: false,
      modalDelete: false,
      formTask: {},
      modelName: 'task',
      keys: [],
      fields: {},
      currentPage: 'task',
    }

    this.getTasks = this.getTasks.bind(this);
    this.onUpdate = this.onUpdate.bind(this);
    this.onDel    = this.onDel.bind(this);
    this.onAdd = this.onAdd.bind(this);
    this.handleCloseModal = this.handleCloseModal.bind(this);
    this.handleShowTask   = this.handleShowTask.bind(this);
    this.handleChange     = this.handleChange.bind(this);
  }

  componentDidMount() {
    this.context.api.desctiption(this.state.modelName)
    .then(body => this.setState(() => ({
      keys: body.keys || [],
      fields: body.fields || {},
      isLoadding: false,
      isError: false,
    })))
    .then(this.getTasks)
    .catch(err => this.setState(() => ({error: err.message})));

  }

  getTasks(filter) {
    if (!filter) {
      filter = this.props.filter;
    }

    return this.context.api.list(this.state.modelName, filter)
    .then(response => {
      return this.setState(() => ({
        tasks: response,
        isLoading: false,
        isError: false
      }));
    })
    .catch(err => 
      this.setState(() => ({isError: true, isLoading: false}))
    )
  }


  onUpdate() {
    return this.context.api
    .update(this.state.modelName, this.state.formTask, {
      id: this.state.formTask.id
    })
    .then(() => this.setState(state => ({
      modal: false,
      madalNew: false,
      tasks: state.tasks
        .map(task => (task.id === state.formTask.id)? state.formTask: task)
    })))
    .catch(console.log);
  }

  onDel() {
    return this.context.api
    .del(this.state.modelName, this.state.formTask.id)
    .then(() => this.setState(state => ({
      modal: false,
      madalNew: false,
      modalDelete:false,
      tasks: state.tasks.filter(task => task.id !== state.formTask.id)
    })))
    .catch(console.log);
  }

  onAdd(set) {
    for(var key in set) {
      if(set[key] === '') {
        return null;
      }
    }
    this.context.api.add(this.state.modelName, set)
    .then(() => this.getTasks())
    .then(() => this.handleCloseModal())
    .catch(console.log);
  }

  handleChange(key, value) {
    console.log(key, value);
    return this.setState(() => ({
      formTask: {
        ...this.state.formTask,
        [key]: value
      } 
    }))
  }

  
  handleShowTask(target, id) {
    if(target.classList.contains('Timer-btn')) {
      return undefined;
    }

    const task = this.state.tasks
      .find(task => task.id === id);

    this.setState(() => ({formTask: task, modal: true, modalNew: false}));
  }

  handleCloseModal() {
    this.setState(() => ({
      modal: false,
      modalNew: false
    }))
  }

  handleChangeFilter(key, value) {
    if (this.props.onChangeFilter) {
      this.props.onChangeFilter(key, value);
    }

    this.getTasks({...this.props.filter, [key]:value});
  }

  render() {
    console.log(this.state, this.props);
    return (
      <div className="App">
        <div className="Page">
          <div className="Header">
            <nav className="navbar navbar-light bg-light">
              <a className="navbar-brand" href="/">Time Tracker</a>
              <div className="btn-group">
                <button className="btn btn-outline-success"
                  type="button"
                  onClick={() => this.setState({
                    modal: true,
                    modalNew: true,
                    formTask: {
                      client_id: this.props.filter.client_id,
                      category_id: this.props.filter.category_id,
                      date_create: new Date().getTime()
                    }
                  })}
                >New Task</button>
                <button className="btn btn-outline-success"
                  type="button"
                  onClick={() => {
                    this.context.api
                    .exportExcel(this.state.modelName, this.props.filter)
                    .then(console.log)
                    .catch(console.log)
                  }}
                >Export excel</button>
                <button className="btn btn-outline-danger"
                  type="button"
                  onClick={() => this.context.signOff()}
                >Sign out</button>
              </div>
            </nav>
          </div>
          <div className="Nav">
            <FilterDate 
              onChange={date_create => 
                this.handleChangeFilter('date_create', date_create)}
            />
            <Filter
              label={"Filter category"}
              value={this.props.filter.category_id}
              modelName="category"
              onChange={(key, value) =>
                this.handleChangeFilter('category_id', value)}
            />
            <Filter
              label={"Filter client"}
              value={this.props.filter.client_id}
              modelName="client"
              filter={{category_id: this.props.filter.category_id}}
              onChange={(key, value) =>
                this.handleChangeFilter('client_id', value)}
            />
            <Filter
              label={"Set price"}
              modelName="price"
            />
            <button type="button"
              className="btn btn-block btn-outline-primary"
              onClick={() => this.props.setPage('statistics')}
            >
              statistics
            </button>
          </div>
          <div className="Article">
            <Tasks
              tasks={this.state.tasks}
              error={this.state.isError}
              loading={this.state.isLoading}
              onShowTask={this.handleShowTask}
            />
          </div>
        </div>
        <IF status={this.state.modalNew && this.state.modal}>
          <Modal show={this.state.modal} onClick={this.handleCloseModal}>
            <h5 key="title" className="modal-title">New task</h5>
            <Form
              row={this.state.formTask}
              keys={this.state.keys}
              fields={this.state.fields}
              onChange={this.handleChange}
            />
            <button key="add" 
              onClick={() => this.onAdd(this.state.formTask)}
              className="btn btn-success"
            >Add</button>
          </Modal>
        </IF>
        <IF status={!this.state.modalNew && this.state.modal}>
          <Modal show={this.state.modal} onClick={this.handleCloseModal}>
            <h5 key="title" className="modal-title">Show task</h5>
            <Form
              row={this.state.formTask}
              keys={this.state.keys}
              fields={this.state.fields}
              onChange={this.handleChange}
            />
            <button key="delete" 
              onClick={() => this.setState(() => ({modalDelete: true}))}
              className="btn btn-danger"
            >Delete</button>,
            <button key="update" 
              onClick={this.onUpdate} 
              className="btn btn-success"
            >Update</button>
          </Modal>
        </IF>
        <IF status={this.state.modalDelete}>
          <ModalFormDelete
            show={this.state.modalDelete}
            modelName={this.state.modelName}
            id={this.state.formTask.id}
            onDelete={this.onDel}
            onClose={() => this.setState(() => ({modalDelete: false}))}
          />
        </IF>
      </div>
    );
  }
}

PageTask.contextType = AuthContext;

export default PageTask;
